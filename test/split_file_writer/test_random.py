import zipfile
from io import BytesIO
import glob
import random
import os
import pathlib
import pytest
import shutil

from split_file_writer import SplitFileWriter, splitlike_file_generator, counting_file_generator
from split_file_reader import SplitFileReader

in_dir = os.path.join(os.getcwd(), "test", "files", "archives")
out_dir = os.path.join(os.getcwd(), "test", "files", "test_output")


def randbytes(n):
    """Generate n random bytes."""
    random.seed(742)
    return random.getrandbits(n * 8).to_bytes(n, 'little')


def test_random_filenames_list_too_small():
    shutil.rmtree(out_dir, ignore_errors=True)
    os.makedirs(out_dir, exist_ok=True)
    files = glob.glob(os.path.join(out_dir, "random.bin*"))
    assert len(files) == 0
    f1 = os.path.join(out_dir, "random.bin.1")
    f2 = os.path.join(out_dir, "random.bin.2")
    f3 = os.path.join(out_dir, "random.bin.3")
    with SplitFileWriter([f1, f2, f3], 50) as sfw:
        with pytest.raises(StopIteration) as ex:
            sfw.write(randbytes(175))
    files = glob.glob(os.path.join(out_dir, "random.bin.*"))
    shutil.rmtree(out_dir)
    assert len(files) == 3


def test_integer_sequence_filename():
    shutil.rmtree(out_dir, ignore_errors=True)
    os.makedirs(out_dir, exist_ok=True)
    files = glob.glob(os.path.join(out_dir, "random.bin.[0-9]*"))
    assert len(files) == 0
    path = pathlib.Path(os.path.join(out_dir, "random.bin."))
    rand = randbytes(25)
    print()
    print(rand)
    with SplitFileWriter(counting_file_generator(path), 5) as sfw:
        sfw.write(rand)
    files = glob.glob(os.path.join(out_dir, "random.bin.[0-9]*"))
    assert len(files) == 5
    shutil.rmtree(out_dir)
    files = glob.glob(os.path.join(out_dir, "random.bin.[0-9]*"))
    assert len(files) == 0


def test_splitlike_filenames():
    shutil.rmtree(out_dir, ignore_errors=True)
    os.makedirs(out_dir, exist_ok=True)
    files = glob.glob(os.path.join(out_dir, "random.bin*"))
    assert len(files) == 0
    path = pathlib.Path(os.path.join(out_dir, "random.bin."))
    rand = randbytes(175)
    print()
    print(rand)
    with SplitFileWriter(splitlike_file_generator(path), 50) as sfw:
        sfw.write(rand)
    files = glob.glob(os.path.join(out_dir, "random.bin.*"))
    with SplitFileReader(files) as sfr:
        read = sfr.read()
        print(read)
        assert read == rand
    shutil.rmtree(out_dir)
    assert len(files) == 4


def test_write_str():
    """
    Premised on the SO question:
    https://stackoverflow.com/questions/68983459/split-a-zip-file-into-chunks-with-python

    :return:
    """
    xml_file = """<xml>
    <entry>
        <something>1</something>
        <something>2</something>
        <something>3</something>
    </entry>
</xml>"""
    chunks = []

    def gen(lst):
        while True:
            lst.append(BytesIO())
            yield lst[-1]

    with SplitFileWriter(gen(chunks), 64) as sfw:
        with zipfile.ZipFile(sfw, "w") as zip_file:
            zip_file.writestr("test.xml", xml_file)

    assert len(chunks) == 5
    chunks[0].seek(0)
    assert chunks[0].read()[:2] == b"PK"

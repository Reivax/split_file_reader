import glob
import hashlib
import json
import logging
import os
import pytest
import shutil
import zipfile

from split_file_writer import SplitFileWriter, counting_file_generator, splitlike_file_generator

in_dir = os.path.join(os.getcwd(), "test", "files", "archives")
out_dir = os.path.join(os.getcwd(), "test", "files", "test_output")


def test_zip_stream_to_files():
    os.makedirs(out_dir, exist_ok=True)
    files = glob.glob(os.path.join(out_dir, "*zip*"))
    assert len(files) == 0
    with SplitFileWriter(counting_file_generator(os.path.join(out_dir, "split.zip."), 0, 4), 500_000) as sfw:
        with zipfile.ZipFile(file=sfw, mode='w') as zipf:
            for root, dirs, files in os.walk(in_dir):
                for file in files:
                    if file.startswith("random_payload"):
                        zipf.write(os.path.join(root, file),
                                   os.path.relpath(os.path.join(root, file),
                                                   os.path.join(in_dir, '..')))

    print(out_dir)
    files = glob.glob(os.path.join(out_dir, "*zip*"))
    shutil.rmtree(out_dir)
    assert len(files) == 3


def test_filenames_str():
    os.makedirs(out_dir, exist_ok=True)
    files = glob.glob(os.path.join(out_dir, "*zip*"))
    assert len(files) == 0
    with SplitFileWriter(os.path.join(out_dir, "split.zip."), 500_000) as sfw:
        with zipfile.ZipFile(file=sfw, mode='w') as zipf:
            for root, dirs, files in os.walk(in_dir):
                for file in files:
                    if file.startswith("random_payload"):
                        zipf.write(os.path.join(root, file),
                                   os.path.relpath(os.path.join(root, file),
                                                   os.path.join(in_dir, '..')))
    files = glob.glob(os.path.join(out_dir, "*zip*"))
    shutil.rmtree(out_dir)
    assert len(files) == 3


def test_filenames_list():
    os.makedirs(out_dir, exist_ok=True)
    files = glob.glob(os.path.join(out_dir, "*zip*"))
    assert len(files) == 0
    f1 = os.path.join(out_dir, "split.zip.1")
    f2 = os.path.join(out_dir, "split.zip.2")
    f3 = os.path.join(out_dir, "split.zip.3")
    with SplitFileWriter([f1, f2, f3], 500_000) as sfw:
        with zipfile.ZipFile(file=sfw, mode='w') as zipf:
            for root, dirs, files in os.walk(in_dir):
                for file in files:
                    if file.startswith("random_payload"):
                        zipf.write(os.path.join(root, file),
                                   os.path.relpath(os.path.join(root, file),
                                                   os.path.join(in_dir, '..')))
    files = glob.glob(os.path.join(out_dir, "*zip*"))
    shutil.rmtree(out_dir)
    assert len(files) == 3


def test_filenames_list_too_small():
    os.makedirs(out_dir, exist_ok=True)
    files = glob.glob(os.path.join(out_dir, "*zip*"))
    assert len(files) == 0
    f1 = os.path.join(out_dir, "split.zip.1")
    f2 = os.path.join(out_dir, "split.zip.2")
    f3 = os.path.join(out_dir, "split.zip.3")
    with pytest.raises(StopIteration) as ex:
        with SplitFileWriter([f1, f2, f3], 500) as sfw:
            with zipfile.ZipFile(file=sfw, mode='w') as zipf:
                for root, dirs, files in os.walk(in_dir):
                    for file in files:
                        if file.startswith("random_payload"):
                            zipf.write(os.path.join(root, file),
                                       os.path.relpath(os.path.join(root, file),
                                                       os.path.join(in_dir, '..')))

    files = glob.glob(os.path.join(out_dir, "*zip*"))
    shutil.rmtree(out_dir)
    assert len(files) == 3


def test_splitlike_file_generator():
    os.makedirs(out_dir, exist_ok=True)
    files = glob.glob(os.path.join(out_dir, "*zip*"))
    assert len(files) == 0
    with SplitFileWriter(splitlike_file_generator(prefix=os.path.join(out_dir, "archive.zip.")), 500_000) as sfw:
        with zipfile.ZipFile(file=sfw, mode='w') as zipf:
            for root, dirs, files in os.walk(in_dir):
                for file in files:
                    if file.startswith("random_payload"):
                        zipf.write(os.path.join(root, file),
                                   os.path.relpath(os.path.join(root, file),
                                                   os.path.join(in_dir, '..')))
    files = glob.glob(os.path.join(out_dir, "*zip*"))
    shutil.rmtree(out_dir)
    assert len(files) == 3


def test_stream_and_hash():
    """
    Expected output:
    {
        "ZIP_NAMES": [
            "split.zip.001",
            "split.zip.002",
            "split.zip.003"
        ],
        "ZIP_HASHES": [
            {
                "file": "split.zip.001",
                "md5": "59a4150a2851c8ca0e4943f9b753ec84"
            },
            {
                "file": "split.zip.002",
                "md5": "4f941994430859daea3195c668cdb318"
            },
            {
                "file": "split.zip.003",
                "md5": "f97ceca2a6a90427649c2d811716a2f5"
            }
        ]
    }

    Process finished with exit code 0

    :return:
    """
    def hashing_file_generator(prefix, _hash_dict):
        """
        Yield out a file pointer, one at a time.
        Upon completion of a given file write, read the file back, and generate a hash of it.
        Update the `hash_dict` to contain the hash and the filename.
        :param prefix: A full path, `/path/to/file/name.zip.`, to be suffixed with a 3 digit number.
        :param _hash_dict: A dict, that will be updated as the files complete.
        :return:
        """
        idx = 0
        _hash_dict["ZIP_NAMES"] = []
        _hash_dict["ZIP_HASHES"] = []
        while True:
            idx += 1
            # Construct the filename, fully qualified
            name = "{}{:03d}".format(prefix, idx)
            # Add it to the list of zip parts.
            _hash_dict['ZIP_NAMES'].append(os.path.basename(name))
            # Open the file handle.
            try:
                with open(name, mode='wb') as file_ptr:
                    # Yield it back, for the writer.
                    yield file_ptr
            except GeneratorExit:
                # When the SplitFileWriter is done, and will be requesting no more files, and is finished with the
                # last file, the SFW will call `close()` on this generator, which in turn raises a `GeneratorExit`
                break
            finally:
                # Load the files back from disk and generate a hash of it.
                split_hasher = hashlib.md5()
                with open(name, mode='rb') as file_ptr:
                    # Iteratively read it.
                    while True:
                        chunk = file_ptr.read(8192)
                        if not chunk:
                            break
                        # And update the hash engine.
                        split_hasher.update(chunk)
                # Complete the hashing and save
                actual_hash = split_hasher.hexdigest().lower()
                # update the dict with the hash values.
                _hash_dict["ZIP_HASHES"].append(
                    {
                        "file": os.path.basename(name),
                        "md5": actual_hash
                    }
                )

    # out_dir = os.path.join(os.getcwd(), "test", "files", "test_output")
    # in_dir = os.path.join(os.getcwd(), "test", "files", "archives")
    os.makedirs(out_dir, exist_ok=True)
    files = glob.glob(os.path.join(out_dir, "*zip*"))
    assert len(files) == 0
    hash_dict = {}
    with SplitFileWriter(
        # First is the generator.
        hashing_file_generator(os.path.join(out_dir, "split.zip."), hash_dict),
        # Next is the filesize.
        500_000
    ) as sfw:
        with zipfile.ZipFile(file=sfw, mode='w') as zipf:
            for root, dirs, files in os.walk(in_dir):
                for file in files:
                    if file.startswith("random_payload"):
                        zipf.write(os.path.join(root, file),
                                   os.path.relpath(os.path.join(root, file),
                                                   os.path.join(in_dir, '..')))
    print(json.dumps(hash_dict, indent='\t'))
    files = glob.glob(os.path.join(out_dir, "*zip*"))
    assert len(files) == 3
    shutil.rmtree(out_dir)
    assert len(hash_dict["ZIP_NAMES"]) == 3
    assert len(hash_dict["ZIP_HASHES"]) == 3


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logging.basicConfig(level=logging.DEBUG)
    os.chdir('../..')

    out_dir = os.path.join(os.getcwd(), "test", "files", "test_output")
    in_dir = os.path.join(os.getcwd(), "test", "files", "archives")

    if os.path.exists(out_dir):
        shutil.rmtree(out_dir)

    test_zip_stream_to_files()
    test_stream_and_hash()
    test_filenames_str()
    test_filenames_list()
    test_filenames_list_too_small()
    # if os.path.exists(out_dir):
    #     shutil.rmtree(out_dir)

import hashlib

expected_hash = "7bf855f82dc4af7ee34d65b662bbc58e"
expected_size = 167544


def test_text_files_are_consistent():
    with open("./test/files/plaintext/Adventures_In_Wonderland.txt", 'rb') as b:
        hasher_b = hashlib.md5()
        while True:
            cb = b.read(1)
            hasher_b.update(cb)
            if not cb:
                break
        hash_b = hasher_b.hexdigest().lower()
        print(hash_b)
        print(expected_hash)
        assert hash_b == expected_hash


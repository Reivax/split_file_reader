import hashlib
import io
import pytest
import tarfile
import zipfile

import requests

from http_file_reader import HTTPFileReader

"""
aff4376de65da6550fc8427d13683b7b *files.tar
4985472dd36295a3013d05298ab4212c *files.tar.000
60482ee4b68a657b915c2c8e4e244aa9 *files.tar.001
8ff44ef8e00277b103cc3ba2dae897e2 *files.tar.002
27e6b0def02cb0c9fc4d73bfb0e2eb42 *files.tar.003
321d5980ffc4f39d6f0640cd462577df *files.zip
b16fc3d4464bdedbf41758137041cf60 *files.zip.000
78a95b9a8e1665a7e6f9bd40aa2ef982 *files.zip.001
1fe06f3165fafd49cdc717c4f397e045 *files.zip.002
32839925d10db0b3e46699d2eb8b18e1 *random_payload_1.bin
7fabb8f1149e9b467429205c9d3d5bcc *random_payload_2.bin
2b8ccbb2958ba4186e4c842826c32435 *random_payload_3.bin
a967ed43b9c62312362dd7cb5334bbdb *random_payload_4.bin
12ce44152c2e5b9a020edbb76cc7ec89 *random_payload_5.bin
"""

url_zip = "https://gitlab.com/Reivax/split_file_reader/-/raw/master/test/files/archives/files.zip?inline=false"
url_tar = "https://gitlab.com/Reivax/split_file_reader/-/raw/master/test/files/archives/files.tar?inline=false"
url_alice = "https://gitlab.com/Reivax/split_file_reader/-/raw/master/test/files/plaintext/Adventures_In_Wonderland.txt?inline=false"


def test_read_zip_from_gitlab():
    with requests.Session() as ses:
        with HTTPFileReader(
            url=url_zip,
            session=ses,
        ) as hfr:
            with zipfile.ZipFile(file=hfr, mode='r') as zf:
                zff = zf.open("random_payload_1.bin")
                hasher = hashlib.md5()
                hasher.update(zff.read())
                zff_payload_actual_hash = hasher.hexdigest().lower()
                assert zff_payload_actual_hash == "32839925d10db0b3e46699d2eb8b18e1"


def test_read_tar_from_gitlab():
    with requests.Session() as ses:
        with HTTPFileReader(
            url=url_tar,
            session=ses,
        ) as hfr:
            with tarfile.TarFile(fileobj=hfr, mode='r') as tf:
                tff = tf.extractfile("random_payload_2.bin")
                hasher = hashlib.md5()
                hasher.update(tff.read())
                tff_payload_actual_hash = hasher.hexdigest().lower()
                assert tff_payload_actual_hash == "7fabb8f1149e9b467429205c9d3d5bcc"


def test_read_tar_from_gitlab():
    with requests.Session() as ses:
        with HTTPFileReader(
            url=url_tar,
            session=ses,
        ) as hfr:
            buff1 = bytearray(100)
            hfr.readinto(buff1)
            hfr.seek(0)
            buff2 = hfr.read(100)
            assert buff1 == buff2


def test_inappropriate_reads():
    with requests.Session() as ses:
        with HTTPFileReader(
            url=url_tar,
            session=ses,
        ) as hfr:
            with pytest.raises(io.UnsupportedOperation):
                _ = hfr.readline()
            with pytest.raises(io.UnsupportedOperation):
                for _ in hfr.readlines():
                    pass
            assert not hfr.writable()
            with pytest.raises(io.UnsupportedOperation):
                hfr.write("Can't do it.")
            with pytest.raises(io.UnsupportedOperation):
                hfr.writelines(["Can't do it."])
            with pytest.raises(io.UnsupportedOperation):
                hfr.truncate(5)
            assert not hfr.isatty()
            with pytest.raises(IOError):
                hfr.fileno()


if __name__ == "__main__":
    # logging.basicConfig(level=logging.DEBUG)
    test_read_zip_from_gitlab()
    test_read_tar_from_gitlab()

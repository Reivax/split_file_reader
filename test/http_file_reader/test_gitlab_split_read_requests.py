import hashlib
import tarfile
import zipfile

import requests

from split_file_reader import SplitFileReader
from http_file_reader import HTTPFileReader


url_zip_split = "https://gitlab.com/Reivax/split_file_reader/-/raw/master/test/files/archives/files.zip.{:03d}?" \
                "inline=false"
url_tar_split = "https://gitlab.com/Reivax/split_file_reader/-/raw/master/test/files/archives/files.tar.{:03d}?" \
                "inline=false"


def test_read_split_zip_from_gitlab():
    """
    So the SplitFileReader can read arbitrary file-like objects, and the HTTPFileReader are file-like objects, we can
    use a list of HTTPFileReaders to process remotely split files and access just individual files.
    :return:
    """
    with requests.Session() as ses:
        with HTTPFileReader(url=url_zip_split.format(0), session=ses, ) as hfr0, \
                HTTPFileReader(url=url_zip_split.format(1), session=ses, ) as hfr1, \
                HTTPFileReader(url=url_zip_split.format(2), session=ses, ) as hfr2:
            with SplitFileReader(
                files=[hfr0, hfr1, hfr2, ]
            ) as sfr:
                with zipfile.ZipFile(file=sfr, mode='r') as zf:
                    zff = zf.open("random_payload_3.bin")
                    hasher = hashlib.md5()
                    hasher.update(zff.read())
                    zff_payload_actual_hash = hasher.hexdigest().lower()
                    assert zff_payload_actual_hash == "2b8ccbb2958ba4186e4c842826c32435"


def test_read_split_tar_from_gitlab():
    with requests.Session() as ses:
        hfrs = []
        for i in range(0, 4):
            hfrs.append(HTTPFileReader(url=url_tar_split.format(i), session=ses, ))
        with SplitFileReader(files=hfrs) as sfr, tarfile.TarFile(fileobj=sfr, mode='r') as tf:
            tff = tf.extractfile("random_payload_4.bin")
            hasher = hashlib.md5()
            hasher.update(tff.read())
            tff_payload_actual_hash = hasher.hexdigest().lower()
            assert tff_payload_actual_hash == "a967ed43b9c62312362dd7cb5334bbdb"


def test_read_split_zip_generated():
    """
    So the SplitFileReader can read arbitrary file-like objects, and the HTTPFileReader are file-like objects, we can
    use a life of HTTPFileReaders to process remotely split files and access just individual files.
    :return:
    """
    with requests.Session() as ses:
        with SplitFileReader(
                files=[HTTPFileReader(url=url_zip_split.format(x), session=ses, ) for x in range(0, 3)]
        ) as sfr:
            with zipfile.ZipFile(file=sfr, mode='r') as zf:
                zff = zf.open("random_payload_5.bin")
                hasher = hashlib.md5()
                hasher.update(zff.read())
                zff_payload_actual_hash = hasher.hexdigest().lower()
                assert zff_payload_actual_hash == "12ce44152c2e5b9a020edbb76cc7ec89"


if __name__ == "__main__":
    # logging.basicConfig(level=logging.DEBUG)
    test_read_split_zip_from_gitlab()
    test_read_split_tar_from_gitlab()
    test_read_split_zip_generated()

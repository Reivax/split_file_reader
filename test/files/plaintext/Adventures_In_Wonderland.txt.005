 over other
children she knew, who might do very well as pigs, and was just saying
to herself, 'if one only knew the right way to change them--' when she
was a little startled by seeing the Cheshire Cat sitting on a bough of a
tree a few yards off.

The Cat only grinned when it saw Alice. It looked good-natured, she
thought: still it had VERY long claws and a great many teeth, so she
felt that it ought to be treated with respect.

'Cheshire Puss,' she began, rather timidly, as she did not at all know
whether it would like the name: however, it only grinned a little wider.
'Come, it's pleased so far,' thought Alice, and she went on. 'Would you
tell me, please, which way I ought to go from here?'

'That depends a good deal on where you want to get to,' said the Cat.

'I don't much care where--' said Alice.

'Then it doesn't matter which way you go,' said the Cat.

'--so long as I get SOMEWHERE,' Alice added as an explanation.

'Oh, you're sure to do that,' said the Cat, 'if you only walk long
enough.'

Alice felt that this could not be denied, so she tried another question.
'What sort of people live about here?'

'In THAT direction,' the Cat said, waving its right paw round, 'lives
a Hatter: and in THAT direction,' waving the other paw, 'lives a March
Hare. Visit either you like: they're both mad.'

'But I don't want to go among mad people,' Alice remarked.

'Oh, you can't help that,' said the Cat: 'we're all mad here. I'm mad.
You're mad.'

'How do you know I'm mad?' said Alice.

'You must be,' said the Cat, 'or you wouldn't have come here.'

Alice didn't think that proved it at all; however, she went on 'And how
do you know that you're mad?'

'To begin with,' said the Cat, 'a dog's not mad. You grant that?'

'I suppose so,' said Alice.

'Well, then,' the Cat went on, 'you see, a dog growls when it's angry,
and wags its tail when it's pleased. Now I growl when I'm pleased, and
wag my tail when I'm angry. Therefore I'm mad.'

'I call it purring, not growling,' said Alice.

'Call it what you like,' said the Cat. 'Do you play croquet with the
Queen to-day?'

'I should like it very much,' said Alice, 'but I haven't been invited
yet.'

'You'll see me there,' said the Cat, and vanished.

Alice was not much surprised at this, she was getting so used to queer
things happening. While she was looking at the place where it had been,
it suddenly appeared again.

'By-the-bye, what became of the baby?' said the Cat. 'I'd nearly
forgotten to ask.'

'It turned into a pig,' Alice quietly said, just as if it had come back
in a natural way.

'I thought it would,' said the Cat, and vanished again.

Alice waited a little, half expecting to see it again, but it did not
appear, and after a minute or two she walked on in the direction in
which the March Hare was said to live. 'I've seen hatters before,' she
said to herself; 'the March Hare will be much the most interesting, and
perhaps as this is May it won't be raving mad--at least not so mad as
it was in March.' As she said this, she looked up, and there was the Cat
again, sitting on a branch of a tree.

'Did you say pig, or fig?' said the Cat.

'I said pig,' replied Alice; 'and I wish you wouldn't keep appearing and
vanishing so suddenly: you make one quite giddy.'

'All right,' said the Cat; and this time it vanished quite slowly,
beginning with the end of the tail, and ending with the grin, which
remained some time after the rest of it had gone.

'Well! I've often seen a cat without a grin,' thought Alice; 'but a grin
without a cat! It's the most curious thing I ever saw in my life!'

She had not gone much farther before she came in sight of the house
of the March Hare: she thought it must be the right house, because the
chimneys were shaped like ears and the roof was thatched with fur. It
was so large a house, that she did not like to go nearer till she had
nibbled some more of the lefthand bit of mushroom, and raised herself to
about two feet high: even then she walked up towards it rather timidly,
saying to herself 'Suppose it should be raving mad after all! I almost
wish I'd gone to see the Hatter instead!'




CHAPTER VII. A Mad Tea-Party

There was a table set out under a tree in front of the house, and the
March Hare and the Hatter were having tea at it: a Dormouse was sitting
between them, fast asleep, and the other two were using it as a
cushion, resting their elbows on it, and talking over its head. 'Very
uncomfortable for the Dormouse,' thought Alice; 'only, as it's asleep, I
suppose it doesn't mind.'

The table was a large one, but the three were all crowded together at
one corner of it: 'No room! No room!' they cried out when they saw Alice
coming. 'There's PLENTY of room!' said Alice indignantly, and she sat
down in a large arm-chair at one end of the table.

'Have some wine,' the March Hare said in an encouraging tone.

Alice looked all round the table, but there was nothing on it but tea.
'I don't see any wine,' she remarked.

'There isn't any,' said the March Hare.

'Then it wasn't very civil of you to offer it,' said Alice angrily.

'It wasn't very civil of you to sit down without being invited,' said
the March Hare.

'I didn't know it was YOUR table,' said Alice; 'it's laid for a great
many more than three.'

'Your hair wants cutting,' said the Hatter. He had been looking at Alice
for some time with great curiosity, and this was his first speech.

'You should learn not to make personal remarks,' Alice said with some
severity; 'it's very rude.'

The Hatter opened his eyes very wide on hearing this; but all he SAID
was, 'Why is a raven like a writing-desk?'

'Come, we shall have some fun now!' thought Alice. 'I'm glad they've
begun asking riddles.--I believe I can guess that,' she added aloud.

'Do you mean that you think you can find out the answer to it?' said the
March Hare.

'Exactly so,' said Alice.

'Then you should say what you mean,' the March Hare went on.

'I do,' Alice hastily replied; 'at least--at least I mean what I
say--that's the same thing, you know.'

'Not the same thing a bit!' said the Hatter. 'You might just as well say
that "I see what I eat" is the same thing as "I eat what I see"!'

'You might just as well say,' added the March Hare, 'that "I like what I
get" is the same thing as "I get what I like"!'

'You might just as well say,' added the Dormouse, who seemed to be
talking in his sleep, 'that "I breathe when I sleep" is the same thing
as "I sleep when I breathe"!'

'It IS the same thing with you,' said the Hatter, and here the
conversation dropped, and the party sat silent for a minute, while Alice
thought over all she could remember about ravens and writing-desks,
which wasn't much.

The Hatter was the first to break the silence. 'What day of the month
is it?' he said, turning to Alice: he had taken his watch out of his
pocket, and was looking at it uneasily, shaking it every now and then,
and holding it to his ear.

Alice considered a little, and then said 'The fourth.'

'Two days wrong!' sighed the Hatter. 'I told you butter wouldn't suit
the works!' he added looking angrily at the March Hare.

'It was the BEST butter,' the March Hare meekly replied.

'Yes, but some crumbs must have got in as well,' the Hatter grumbled:
'you shouldn't have put it in with the bread-knife.'

The March Hare took the watch and looked at it gloomily: then he dipped
it into his cup of tea, and looked at it again: but he could think of
nothing better to say than his first remark, 'It was the BEST butter,
you know.'

Alice had been looking over his shoulder with some curiosity. 'What a
funny watch!' she remarked. 'It tells the day of the month, and doesn't
tell what o'clock it is!'

'Why should it?' muttered the Hatter. 'Does YOUR watch tell you what
year it is?'

'Of course not,' Alice replied very readily: 'but that's because it
stays the same year for such a long time together.'

'Which is just the case with MINE,' said the Hatter.

Alice felt dreadfully puzzled. The Hatter's remark seemed to have no
sort of meaning in it, and yet it was certainly English. 'I don't quite
understand you,' she said, as politely as she could.

'The Dormouse is asleep again,' said the Hatter, and he poured a little
hot tea upon its nose.

The Dormouse shook its head impatiently, and said, without opening its
eyes, 'Of course, of course; just what I was going to remark myself.'

'Have you guessed the riddle yet?' the Hatter said, turning to Alice
again.

'No, I give it up,' Alice replied: 'what's the answer?'

'I haven't the slightest idea,' said the Hatter.

'Nor I,' said the March Hare.

Alice sighed wearily. 'I think you might do something better with the
time,' she said, 'than waste it in asking riddles that have no answers.'

'If you knew Time as well as I do,' said the Hatter, 'you wouldn't talk
about wasting IT. It's HIM.'

'I don't know what you mean,' said Alice.

'Of course you don't!' the Hatter said, tossing his head contemptuously.
'I dare say you never even spoke to Time!'

'Perhaps not,' Alice cautiously replied: 'but I know I have to beat time
when I learn music.'

'Ah! that accounts for it,' said the Hatter. 'He won't stand beating.
Now, if you only kept on good terms with him, he'd do almost anything
you liked with the clock. For instance, suppose it were nine o'clock in
the morning, just time to begin lessons: you'd only have to whisper a
hint to Time, and round goes the clock in a twinkling! Half-past one,
time for dinner!'

('I only wish it was,' the March Hare said to itself in a whisper.)

'That would be grand, certainly,' said Alice thoughtfully: 'but then--I
shouldn't be hungry for it, you know.'

'Not at first, perhaps,' said the Hatter: 'but you could keep it to
half-past one as long as you liked.'

'Is that the way YOU manage?' Alice asked.

The Hatter shook his head mournfully. 'Not I!' he replied. 'We
quarrelled last March--just before HE went mad, you know--' (pointing
with his tea spoon at the March Hare,) '--it was at the great concert
given by the Queen of Hearts, and I had to sing

     "Twinkle, twinkle, little bat!
     How I wonder what you're at!"

You know the song, perhaps?'

'I've heard something like it,' said Alice.

'It goes on, you know,' the Hatter continued, 'in this way:--

     "Up above the world you fly,
     Like a tea-tray in the sky.
         Twinkle, twinkle--"'

Here the Dormouse shook itself, and began singing in its sleep 'Twinkle,
twinkle, twinkle, twinkle--' and went on so long that they had to pinch
it to make it stop.

'Well, I'd hardly finished the first verse,' said the Hatter, 'when the
Queen jumped up and bawled out, "He's murdering the time! Off with his
head!"'

'How dreadfully savage!' exclaimed Alice.

'And ever since that,' the Hatter went on in a mournful tone, 'he won't
do a thing I ask! It's always six o'clock now.'

A bright idea came into Alice's head. 'Is that the reason so many
tea-things are put out here?' she asked.

'Yes, that's it,' said the Hatter with a sigh: 'it's always tea-time,
and we've no time to wash the things between whiles.'

'Then you keep moving round, I suppose?' said Alice.

'Exactly so,' said the Hatter: 'as the things get used up.'

'But what happens when you come to the beginning again?' Alice ventured
to ask.

'Suppose we change the subject,' the March Hare interrupted, yawning.
'I'm getting tired of this. I vote the young lady tells us a story.'

'I'm afraid I don't know one,' said Alice, rather alarmed at the
proposal.

'Then the Dormouse shall!' they both cried. 'Wake up, Dormouse!' And
they pinched it on both sides at once.

The Dormouse slowly opened his eyes. 'I wasn't asleep,' he said in a
hoarse, feeble voice: 'I heard every word you fellows were saying.'

'Tell us a story!' said the March Hare.

'Yes, please do!' pleaded Alice.

'And be quick about it,' added the Hatter, 'or you'll be asleep again
before it's done.'

'Once upon a time there were three little sisters,' the Dormouse began
in a great hurry; 'and their names were Elsie, Lacie, and Tillie; and
they lived at the bottom of a well--'

'What did they live on?' said Alice, who always took a great interest in
questions of eating and drinking.

'They lived on treacle,' said the Dormouse, after thinking a minute or
two.

'They couldn't have done that, you know,' Alice gently remarked; 'they'd
have been ill.'

'So they were,' said the Dormouse; 'VERY ill.'

Alice tried to fancy to herself what such an extraordinary ways of
living would be like, but it puzzled her too much, so she went on: 'But
why did they live at the bottom of a well?'

'Take some more tea,' the March Hare said to Alice, very earnestly.

'I've had nothing yet,' Alice replied in an offended tone, 'so I can't
take more.'

'You mean you can't take LESS,' said the Hatter: 'it's very easy to take
MORE than nothing.'

'Nobody asked YOUR opinion,' said Alice.

'Who's making personal remarks now?' the Hatter asked triumphantly.

Alice did not quite know what to say to this: so she helped herself
to some tea and bread-and-butter, and then turned to the Dormouse, and
repeated her question. 'Why did they live at the bottom of a well?'

The Dormouse again took a minute or two to think about it, and then
said, 'It was a treacle-well.'

'There's no such thing!' Alice was beginning very angrily, but the
Hatter and the March Hare went 'Sh! sh!' and the Dormouse sulkily
remarked, 'If you can't be civil, you'd better finish the story for
yourself.'

'No, please go on!' Alice said very humbly; 'I won't interrupt again. I
dare say there may be ONE.'

'One, indeed!' said the Dormouse indignant
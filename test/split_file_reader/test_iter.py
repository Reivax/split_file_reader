import hashlib
import os
import pytest
import random

from split_file_reader import SplitFileReader

filepaths = [
    "./test/files/plaintext/Adventures_In_Wonderland.txt.000",
    "./test/files/plaintext/Adventures_In_Wonderland.txt.001",
    "./test/files/plaintext/Adventures_In_Wonderland.txt.002",
    "./test/files/plaintext/Adventures_In_Wonderland.txt.003",
    "./test/files/plaintext/Adventures_In_Wonderland.txt.004",
    "./test/files/plaintext/Adventures_In_Wonderland.txt.005",
    "./test/files/plaintext/Adventures_In_Wonderland.txt.006",
    "./test/files/plaintext/Adventures_In_Wonderland.txt.007",
    "./test/files/plaintext/Adventures_In_Wonderland.txt.008",
    "./test/files/plaintext/Adventures_In_Wonderland.txt.009",
    "./test/files/plaintext/Adventures_In_Wonderland.txt.010",
    "./test/files/plaintext/Adventures_In_Wonderland.txt.011",
]
expected_hash = "7bf855f82dc4af7ee34d65b662bbc58e"
expected_size = 167544


def test_iter_comparative():
    with SplitFileReader(filepaths) as s, \
            open("./test/files/plaintext/Adventures_In_Wonderland.txt", 'rb') as f:
        hasher_s = hashlib.md5()
        hasher_f = hashlib.md5()
        while True:
            bs = s.read(1)
            bf = f.read(1)
            assert bs == bf
            hasher_s.update(bs)
            hasher_f.update(bf)
            if not bs or not bf:
                break
        hash_s = hasher_s.hexdigest().lower()
        hash_f = hasher_f.hexdigest().lower()
        assert hash_f == expected_hash == hash_s


def test_iter_simple():
    with SplitFileReader(filepaths) as sfr:
        split_hasher = hashlib.md5()
        for b in sfr:
            split_hasher.update(b)
        actual_hash = split_hasher.hexdigest().lower()
        assert actual_hash == expected_hash


def test_iter_impossible():
    with SplitFileReader(filepaths, stream_only=False) as sfr:
        for byte in sfr:
            with pytest.raises(IOError):
                sfr.seek(-1, 1)
            break


def test_iter_larger():
    with SplitFileReader(filepaths) as sfr:
        split_hasher = hashlib.md5()
        next_read_size = 12
        sfr.set_iter_size(next_read_size)
        for b in sfr:
            split_hasher.update(b)
            # Last read can be short.
            if sfr.tell() != 167544:
                assert len(b) == next_read_size
                assert len(b) >= 10
            assert len(b) < 20
            next_read_size = random.randrange(10, 20)
            sfr.set_iter_size(next_read_size)
            # print(" ".join("{:02X}".format(x) for x in b))
        actual_hash = split_hasher.hexdigest().lower()
        assert actual_hash == expected_hash


if __name__ == "__main__":
    os.chdir('../..')
    test_iter_simple()
    test_iter_larger()

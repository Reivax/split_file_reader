# HTTPFileReader

A python module to transparently read files that are hosted on a remote HTTP
server.  Exposes the `readable`, `read`, `writable`, `write`, `tellable`, 
`tell`, `seekable`, `seek`, `open` and `close` functions, as well as a Context
Manager.

### Usage
#### Simple Example
List all of the files within a Tar file that is remotely hosted.

```python
import tarfile
import requests
from split_file_reader.http_file_reader import HTTPFileReader

with requests.Session() as session:
    with HTTPFileReader(url="http://localhost/file.tar", session=session) as hfr:
        with tarfile.open(fileobj=hfr, mode="r") as tf:
            for tff in tf.filelist:
                print("File in archive: ", tff.name)
```

Download one file from a Zip archive that is remotely hosted:

```python
import requests
import zipfile
from split_file_reader.http_file_reader import HTTPFileReader

with requests.Session() as session:
    with HTTPFileReader(url="https://localhost/files.zip", session=session, ) as hfr:
        with zipfile.ZipFile(file=hfr, mode='r') as zf:
            with open("./random_payload_1.bin", 'w') as outfile:
                zff = zf.open("random_payload_1.bin")
                outfile.write(zff.read())
```

#### Requirements
The HTTP server must support the `Range` header value, as well as behave 
correctly for `HEAD` requests.  Most modern web servers support this natively,
but not all do; servers that do not support `Range` will return the entire
file.

The `requests` module is used to manage the HTTP traffic; therefore you must
construct your own HTTP Session.  The syntax is simple, and will maintain a 
single connection to the HTTP server for entire time, making all the calls
faster.

HTTPS is supported as well.

#### Cookies and session state
This class makes use of the `requests` module.  To assign cookies, store them 
in the `session` object, or load them into the `headers` parameter.
A session object may also be given special operating criteria, such
as Retry conditions for unreliable hosts, see the `requests` module for 
further documentation.

The `requests.Session()` object automatically tracks and updates Cookie data.

### Use case

Sometimes small amounts of data are hidden within very large files, that would
generally require downloading the entire file.  With the `HTTPFileReader`, the
data can be random-accessed like a typical Python file.  This is especially
useful for remote Zip or Tar files.  
With the `HTTPFileReader`, each piece of data can be streamed into its own
file, and then used directly, without the need to first download the archive.

#### Optimizations:
Accessing few items from archives provides the best performance, otherwise
downloading the entire archive is probably more efficient.  Tar files in
streaming mode (`mode='r|'`) do support `seek` for forward movement, but
implement it with a looped call to `read`, so this will not be an efficient 
use.  In fact, this will ultimately download the entire tar file anyway.
Tar files in random-access mode (`mode='r'` or `mode='r:'`) must skip to
every file header in the archive to identify the appropriate file offsets.
For archives with lots of files, this may be expensive, but for archives with 
large, irrelevant files, this may be advantageous.

Zip files, on the other hand, have a central directory at the end of the 
archive.  The central directory is loaded in, and the location of every
payload file is known; individual reads are therefore fast, and few read
operations (generally only 3) are required to find and list every payload.  
`zipfile.read` will then perform a `read` for the payload component. 

#### Github and Gitlab

Github and Gitlab support the HTTP `Range` header option, and are compatible
without any configuration.


#### Constructor Arguments
- `url`: A full URL to the target file on the webserver.  Any URL parameters 
must already be in the string.  Dynamic parameters are not supported, as this
URL will be hit several times.
- `mode`: this must be `rb` or `r`.  It is only left for programs that
explicitly set the `mode` argument.
- `session`: A `requests.Session()` object, used to maintain the HTTP
connection. 
- `headers`: A dict containing any necessary header information for the HTTP
traffic.  `Range` will be overwritten.

#### Context Manager
The `HTTPFileReader` allows for a Context Manager.  It will not close the
`requests.Session()` object that is passed as the `session` parameter.

#### Concurrency
The `HTTPFileReader` is not designed for concurrent or threaded access, it 
behaves the same as any other file that has been opened via `open()`.
However, since the data it operates against is read-only, multiple
`HTTPFileReader`s can be opened against the same data at the same time.

#### Caveats
The HTTP Server must support the `Range` header option, and must return the 
file size in a `HEAD` request.

This library has not been tested with Python2.7, and compatibility was not considered.

# SplitFileWriter

A python module to produce parted files on disk.  Exposes the 
`readable`, `read`, `writable`, `write`, `tellable`, `tell`, `seekable`, `seek`, `open` and `close` functions, as well
as a Context Manager.

### Usage
#### Simple Example
Create a `zip` file of a home directory across multiple output files.

```python
import os
import zipfile
from split_file_reader.split_file_writer import SplitFileWriter

with SplitFileWriter() as sfw:
    with zipfile.ZipFile(file=sfw, mode='w') as zipf:
        for root, dirs, files in os.walk('~'):
            for file in files:
                zipf.write(os.path.join(root, file),
                           os.path.relpath(os.path.join(root, file),
                                           os.path.join(indir, '../split_file_reader_x')))
```

These files may be anywhere on disk, or across multiple disks.

SplitFileWriter does not support writing, `readable` will always return `False`, calls to `read` will raise an
IOError.

#### Text files.
The `SplitFileWriter` works only on binary data, but does support the use of the `io.TextIOWrapper`.
In these scenarios, using the `SplitFileWriter` will provide an alternative solution, enabling streaming writes
throughout the archive without first making a single file on disk.


#### Arguments
##### filenames
If `filenames` is a single `str` or `PathLike`, a default file generator will be created that simply
appends a 3 digit numeric value to the end of the filepath.

Else, If `filenames` is a `list`, the list be assumed to be `str` or `os.PathLike` as described above, and a
file will be opened for each of element.  No suffix will be added.  If the list is too small, a
`StopIteration` exception will be raised at the end of the list.

Else, If `filenames` is None, a default generator that works identically to bsd `split` will be created,
with values of `xaa`, `xab`, `xac`, ... in the current working directory.

Else, `filenames` will be assumed to be a Generator, and SplitFileWriter will call `iter()` on it, then
advance it via `next()`.  This is user supplied, and take any construction you wish, so long as the object it
yields supports `write()` and `close()`

##### max_part_size
The second argument is the size in bytes to split the split the files.  It is by default a half-megabyte, but should be
changed to the size appropriate.

#### Github and Gitlab Large File Size

Github and Gitlab (as well as other file repositories) impose file size limits.  By parting these files into
sufficiently small chunks, the `SplitFileWriter` will be able to make transparent production of them, as though they
were a single cohesive file.  This will remove any requirements to host these files with pre-fetch or pre-commit scripts, or
to enable any other "setup" mechanism to make use of them.  `SplitFileReader` can manage them without `cat`.

## Filename Generators
The `SplitFileWriter` uses a Python generator to produce the filenames.  The `SplitFileWriter` will call `next()` on
the generator to get the next filename, that can be either a relative location or a fully qualified location.

The first argument to the `SplitFileWriter` is the generator function.

### Packaged Generators
#### counting_file_generator
The `counting_file_generator` produces simple filenames.  Given a single string, it simply appends numbers as a suffix.

If `filenames` is a single `str` or `PathLike` object, this is the generator used.

`counting_file_generator(filename: str, start_from=0, width=3)` takes up to three arguments, the basename for the
filename, f.x. `/mnt/file/archive.zip.` a starting index, f.x. `0`, and a width.  It will then produce a series of 
files sequentially increasing numbers of at least the number of digits equal to the width.  In this example,
`counting_file_generator("/mnt/file/archive.zip.")` will produce `/mnt/file/archive.zip.000`,
`/mnt/file/archive.zip.001`, etc.  If more digits are needed to represent the number, it will simply continue
increasing the size of the suffix to fit.


#### file_generator_from_list
If a list of `str` or `PathLike` objects is passed, this is the generator used.  It is quite simple, in that the 
generator simply iterates over every element in the list, calls a context-managed `open` on the element, and yields it.

The signature is `file_generator_from_list(list_of_filenames: typing.List[str]):`

#### splitlike_file_generator
This file generator works identically to the naming convention of BSD `split`s default behavior.  The signature is
`splitlike_file_handle_generator(prefix='x', addl_suffix='')`

If `filenames` is None, this is the generator used.

`split` default names are weird but alphabetical.  Add the filepath to the prefix to write specific places.

Filename prefix of `x`, with two letter suffixes from `aa` to `yz`.\
Then, prefix of `xz`, with three letter suffixes from `aaa` to `yzz`.\
Then, prefix of `xzz`, with four letter suffixes from `aaaa` to `yzzz`.\
Every time this loops, add another `z` to the prefix, then add another alphabet term to the counter.

With the default arguments, it produces files named `xaa`, `xab`, ... `xyy`, `xyz`, `xzaaa`, `xzaab`, ... `xzyzy`, 
`xzyzz`, `xzzaaaa`, `xzzaaab`.  In this case, the prefix is `x`, and may be set to anything, and an additional suffix
may be appended to end of every filename.  The files are placed the in current working directory.

### Custom Output File Generators
Any generator that produces a file-like object is acceptable as the filename generator, and can be written on demand.
Simply pass the generator call as the `filenames` argument.  For example:
```python
SplitFileWriter(simple_file_generator("files.zip."), 500_000) as sfr:
    pass
```

If the generator completes and exits before all data is written, the `write()` method on the `SplitFileWriter` will
raise a `StopIteration` exception.

#### Arbitrary File Generator
The premise of the `counting_file_generator` is this.  It can be changed to your desires.  As long as it `yield`s out a 
file-like object, it will work.  The yield should be within a context manager to ensure it closes correctly in the 
event of an error or the `SplitFileWriter` is closed.
```python
def simple_file_generator(filename):
    idx = 0
    while True:
        name = "{}{:03d}".format(filename, idx)
        with open(name, mode='wb') as file_ptr:
            yield file_ptr
        idx += 1
```

#### Variations
The generators used to produce the output do not need to construct File objects at all; in fact, they need only be 
file-like.  Anything, including `requests` response objects, `io.BufferedWriter`, or similar that allow for the
`write()` method will work.  As soon as the write stream hits the specified number of bytes, control will return to
the generator the object can be closed or otherwise managed, and the generator should yield a new object.

#### Keep a running list of the output files _and_ their hashes.

This is an example of a complex file generator that performs additional work on each file, before and after writing to 
it.  In this case, generate a JSON document with the information about the data produced by the Split File Writer, for
example to ensure integrity for a file transfer mechanism.

Zip an entire directory in a single zip file, but make sure the output is split into chunks no larger than 500kB. 
Additionally, read back the chunk to generate a hash of each file part.  Since this generator must do additional 
work after the last file is closed, it must catch the `GeneratorExit` exception.  The `SplitFileWriter` will
call `close()` on the generator when `close()` is called on it.
```python
import hashlib
import json
import os
import zipfile

from split_file_reader.split_file_writer import SplitFileWriter

# Define the generator
def hashing_file_generator(prefix, hash_dict):
    hash_dict["FileHashes"] = []
    hash_dict["OrderedFiles"] = []
    idx = 0
    while True:
        idx += 1
        # Construct the filename, fully qualified
        name = "{}{:03d}".format(prefix, idx)
        hash_dict['OrderedFiles'].append(name)
        # Open the file handle.
        try:
            with open(name, mode='wb') as file_ptr:
                # Yield it back, for the writer.
                yield file_ptr
        except GeneratorExit:
            # When the SplitFileWriter is done, and will be requesting no more files, and is finished with the
            # last file, the SFW will call `close()` on this generator, which in turn raises a `GeneratorExit`
            break
        finally:
            # (re)load the files back from disk and generate a hash of it.
            split_hasher = hashlib.md5()
            with open(name, mode='rb') as file_ptr:
                # Iteratively read it.
                while True:
                    chunk = file_ptr.read(8192)
                    if not chunk:
                        break
                    # And update the hash engine.
                    split_hasher.update(chunk)
            # Complete the hashing and save
            actual_hash = split_hasher.hexdigest().lower()
            # update the dict with the hash values.
            hash_dict["FileHashes"].append(
                {
                    "file": name,
                    "md5": actual_hash
                }
            )

# Use the generator.
in_dir = os.path.join(os.getcwd(), "test", "files", "archives")
hash_dict = {}
with SplitFileWriter(
    # First is the generator.
    hashing_file_generator("split.zip.", hash_dict),
    # Next is the filesize, in this case, half-megabyte.
    500_000
) as sfw:
    with zipfile.ZipFile(file=sfw, mode='w') as zipf:
        for root, dirs, files in os.walk(in_dir):
            for file in files:
                zipf.write(os.path.join(root, file),
                           os.path.relpath(os.path.join(root, file),
                                           os.path.join(in_dir, '..')))

print(json.dumps(hash_dict, indent='\t'))
```

This will update a dictionary with the following values, for this simple example:
```json
{
	"OrderedFiles": [
		"split.zip.001",
		"split.zip.002",
		"split.zip.003"
	],
	"FileHashes": [
		{
			"file": "split.zip.001",
			"md5": "bef37336a14fcfaddb9fb315bf49e208"
		},
		{
			"file": "split.zip.002",
			"md5": "9f04b1897ff53842fc1f9fba2eeb8cd2"
		},
		{
			"file": "split.zip.003",
			"md5": "f97ceca2a6a90427649c2d811716a2f5"
		}
	]
}
```
